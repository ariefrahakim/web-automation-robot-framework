*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/Live-TV/locatorCatchUp.py

*** Keywords ***
Click Page Live TV
    Selenium2Library.Click Element    ${menu_live_tv}

Press Page Down
    Selenium2Library.Press Keys    None    Page Down

Click Catch Up Program
    Selenium2Library.Click Element    ${catch_up_program}

# Screenshot Catch Up Program Play
#     Selenium2Library.Capture Page Screenshot   ${capture_cu_program_play}

Click Pause Video
    Selenium2Library.Click Element    ${btn_pause}

# Screenshot Catch Up Program Pause
#     Selenium2Library.Capture Page Screenshot   ${capture_cu_program_pause}

Click Continue Video
    Selenium2Library.Click Element    ${btn_continue}

Click Fullscreen
    Selenium2Library.Double Click Element    ${btn_fullscreen}

# Screenshot Catch Up Program Fullscreen
#     Selenium2Library.Capture Page Screenshot   ${capture_cu_program_fs}

Press ESC
    Selenium2Library.Press Keys    None    ESC

Click Pause2 Video
    Selenium2Library.Click Element    ${btn_pause}

Click Continue2 Video
    Selenium2Library.Click Element    ${btn_continue}

Click Other Channels
    Selenium2Library.Click Element    ${INews_channels}

# Screenshot Catch Up Other Channels
#     Selenium2Library.Capture Page Screenshot   ${capture_cu_other_channels}







