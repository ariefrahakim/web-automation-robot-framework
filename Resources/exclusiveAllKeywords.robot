*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/Exclusive/locatorExclusiveAll.py

*** Keywords ***
Click Page Exclusive
    Selenium2Library.Click Element    ${menu_exclusive}

Wait List Menu Exclusive
    Selenium2Library.Wait Until Element Is Visible   ${list_menu_exclusive}

Click All
    Selenium2Library.Click Element    ${menu_all}

Click Play Video
    Selenium2Library.Click Element   ${btn_play}

Click Pause Video
    Selenium2Library.Click Element   ${btn_pause}

Click Continue Video
    Selenium2Library.Click Element   ${btn_continue_play}

Click Close Video
    Selenium2Library.Click Element    ${btn_close_video}
