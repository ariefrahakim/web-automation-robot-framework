*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/News/locatorNewsGayaHidup.py   

*** Keywords ***
Click Page News
    Selenium2Library.Click Element    ${menu_news}

Click Arrow Next
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 2
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 3
    Selenium2Library.Click Element    ${next_arrow}

Click Gaya Hidup
    Selenium2Library.Click Element    ${gaya_hidup}

Click Sample Berita
    Selenium2Library.Click Element    ${sample_berita}

Press Page Down
    Selenium2Library.Press Keys    None    Page Down

Press Page Down 2
    Selenium2Library.Press Keys    None    Page Down

Click Artikel Asli
    Selenium2Library.Click Element    ${artikel_asli}