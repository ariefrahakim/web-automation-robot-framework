*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/Live-Event/locatorLiveEvent.py

*** Keywords ***
Click Page Live Event
    Selenium2Library.Click Element    ${menu_live_event}

Click Pause Video
    Selenium2Library.Click Element    ${btn_pause}

Click Continue Video
    Selenium2Library.Click Element    ${btn_continue}

Click Fullscreen
    Selenium2Library.Double Click Element    ${btn_fullscreen}

Press ESC
    Selenium2Library.Press Keys    None    ESC

Click Pause2 Video
    Selenium2Library.Click Element    ${btn_pause}

Click Continue2 Video
    Selenium2Library.Click Element    ${btn_continue}

Click Live Event Live
    Selenium2Library.Click Element    ${live_event_live}

Click Pause3 Video
    Selenium2Library.Click Element    ${btn_pause_live_event_list}

Click Continue3 Video
    Selenium2Library.Click Element    ${btn_continue_live_event_list}

Click Fullscreen2
    Selenium2Library.Double Click Element    ${btn_full_screen_live_event_list}

Press ESC 2
    Selenium2Library.Press Keys    None    ESC

Click Pause4 Video
    Selenium2Library.Click Element    ${btn_pause_live_event_list}

Click Continue4 Video
    Selenium2Library.Click Element    ${btn_continue_live_event_list}








