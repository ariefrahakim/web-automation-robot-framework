*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/locatorLogout.py

*** Keywords ***
Click Page Profile
    Selenium2Library.Click Element    ${page_profile}

Wait Page Profile   
    Selenium2Library.Wait Until Element Is Visible    ${landing_page_profile}

# Screenshot Logout
#     Selenium2Library.Capture Page Screenshot   ${capture_logout}

Click Logout
    Selenium2Library.Click Element    ${btn_logout}
    
Close all browser
    Selenium2Library.Close All Browsers