*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/News/locatorNewsGlobal.py   

*** Keywords ***
Click Page News
    Selenium2Library.Click Element    ${menu_news}

Click Arrow Next
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 2
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 3
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 4
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 5
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 6
    Selenium2Library.Click Element    ${next_arrow}

Click Arrow Next 7
    Selenium2Library.Click Element    ${next_arrow}

Click Global
    Selenium2Library.Click Element    ${menu_global}

Click Sample Berita
    Selenium2Library.Click Element    ${sample_berita}

Press Page Down
    Selenium2Library.Press Keys    None    Page Down

Press Page Down 2
    Selenium2Library.Press Keys    None    Page Down

Click Artikel Asli
    Selenium2Library.Click Element    ${artikel_asli}