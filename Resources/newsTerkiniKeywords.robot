*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/News/locatorNewsTerkini.py   

*** Keywords ***
Click Page News
    Selenium2Library.Click Element    ${menu_news}

Click Terkini
    Selenium2Library.Click Element    ${terkini}

Click Sample Berita
    Selenium2Library.Click Element    ${sample_berita}

Press Page Down
    Selenium2Library.Press Keys    None    Page Down

Press Page Down 2
    Selenium2Library.Press Keys    None    Page Down

Click Artikel Asli
    Selenium2Library.Click Element    ${artikel_asli}