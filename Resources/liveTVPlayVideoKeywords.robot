*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/Live-TV/locatorLiveTV.py

*** Keywords ***
Click Page Live TV
    Selenium2Library.Click Element    ${menu_live_tv}

Click TV Channels
    Selenium2Library.Click Element    ${INews_channels}

Click Pause Video
    Selenium2Library.Click Element    ${btn_pause}

Click Continue Video
    Selenium2Library.Click Element    ${btn_continue}

Click Fullscreen
    Selenium2Library.Double Click Element    ${btn_fullscreen}

Press ESC
    Selenium2Library.Press Keys    None    ESC

Click Pause2 Video
    Selenium2Library.Click Element    ${btn_pause}

Click Continue2 Video
    Selenium2Library.Click Element    ${btn_continue}







