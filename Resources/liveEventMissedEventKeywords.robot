*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/Live-Event/locatorLiveEventMissedEvent.py

*** Keywords ***
Click Page Live Event
    Selenium2Library.Click Element    ${menu_live_event}

Click Missed Event
    Selenium2Library.Click Element    ${missed_event}

Click Missed Event List
    Selenium2Library.Click Element    ${missed_event_list}

Click Pause Video
    Selenium2Library.Click Element    ${btn_pause}

Click Continue Video
    Selenium2Library.Click Element    ${btn_continue}

Click Fullscreen
    Selenium2Library.Double Click Element    ${btn_fullscreen}

Press ESC
    Selenium2Library.Press Keys    None    ESC

Click Pause2 Video
    Selenium2Library.Click Element    ${btn_pause}

Click Continue2 Video
    Selenium2Library.Click Element    ${btn_continue}







