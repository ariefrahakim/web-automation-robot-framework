*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/Exclusive/locatorExclusivePhoto.py

*** Keywords ***
Click Page Exclusive
    Selenium2Library.Click Element    ${menu_exclusive}

Click Photo
    Selenium2Library.Click Element    ${menu_photo}

Click Right
    Selenium2Library.Click Element   ${btn_next_right}

Click Next Right
    Selenium2Library.Click Element   ${btn_next_right}

Click Back Left
    Selenium2Library.Click Element   ${btn_back_left}

Click Slide Bottom
    Selenium2Library.Click Button    ${btn_slide_bottom}
