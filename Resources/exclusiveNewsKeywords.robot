*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/Exclusive/locatorExclusiveNews.py

*** Keywords ***
Click Page Exclusive
    Selenium2Library.Click Element    ${menu_exclusive}

Click News
    Selenium2Library.Click Element    ${menu_news}

Click Play Video
    Selenium2Library.Click Element   ${btn_play}

Click Pause Video
    Selenium2Library.Click Element   ${btn_pause}

Click Continue Video
    Selenium2Library.Click Element   ${btn_continue_play}

Click Close Video
    Selenium2Library.Click Element    ${btn_close_video}
