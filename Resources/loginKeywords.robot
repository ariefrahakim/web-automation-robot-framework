*** Settings ***
Library    Selenium2Library
Variables  ../ObjectsRepository/locatorLogin.py

*** Keywords ***
Open My Browser
    [Arguments]    ${URL}    ${BROWSER}
    Selenium2Library.Open Browser  ${URL}     ${BROWSER}
    Maximize Browser Window

Enter UserName
    [Arguments]    ${USERNAME}
    Selenium2Library.Input Text    ${txt_loginUserName}    ${USERNAME}

Enter Password
    [Arguments]    ${PASSWORD}
    Selenium2Library.Input Text    ${txt_loginPassword}    ${PASSWORD}

# Screenshot Login
#     Selenium2Library.Capture Page Screenshot        ${capture_login}

Click Login
    Selenium2Library.Click Button    ${btn_login}

Wait Landing Page
    Selenium2Library.Wait Until Element Is Visible    ${landing_page}
    
