*** Settings ***
Library    Selenium2Library
Resource  ../../Resources/loginKeywords.robot 
Resource  ../../Resources/exclusiveBehindTheScenesKeywords.robot
Resource  ../../Resources/logoutKeywords.robot 

*** Variables ***
${BROWSER}      headlesschrome
${URL}          http://rc-webd.rctiplus.com/login
${DELAY}        1s
${DELAY2}       19s
${USER}     qa_test@mailinator.com
${PWD}     dikakoko

*** Test Cases ***
Login in RCTI Plus
    Open My Browser    ${URL}    ${BROWSER}
    Sleep     ${DELAY}
    Enter UserName    ${USER}
    Sleep     ${DELAY}
    Enter Password    ${PWD}
    Sleep    ${DELAY}
    Click Login
    Sleep    ${DELAY}

Menu Exclusive Behind The Scenes

    Click Page Exclusive
    Sleep    ${DELAY}
    Click Behind The Scenes
    Sleep    ${DELAY}
    Click Play Video
    Sleep    ${DELAY2}
    Click Pause Video
    Sleep    ${DELAY}
    Click Continue Video
    Sleep    ${DELAY2}
    Click Close Video
    Sleep    ${DELAY}

Logout in RCTI Plus
    Click Page Profile
    Sleep    ${DELAY}
    Click Logout
    Sleep    ${DELAY}
    Close all browser
