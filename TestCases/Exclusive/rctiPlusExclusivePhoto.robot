*** Settings ***
Library    Selenium2Library
Resource  ../../Resources/loginKeywords.robot 
Resource  ../../Resources/exclusivePhotoKeywords.robot
Resource  ../../Resources/logoutKeywords.robot 

*** Variables ***
${BROWSER}      headlesschrome
${URL}          http://rc-webd.rctiplus.com/login
${DELAY}        1s
${DELAY2}       3s
${USER}     qa_test@mailinator.com
${PWD}     dikakoko

*** Test Cases ***
Login in RCTI Plus
    Open My Browser    ${URL}    ${BROWSER}
    Sleep     ${DELAY}
    Enter UserName    ${USER}
    Sleep     ${DELAY}
    Enter Password    ${PWD}
    Sleep    ${DELAY}
    Click Login
    Sleep    ${DELAY}

Menu Exclusive Photo

    Click Page Exclusive
    Sleep    ${DELAY}
    Click Photo
    Sleep    ${DELAY}
    Click Right
    Sleep    ${DELAY}
    Click Next Right
    Sleep    ${DELAY}
    Click Back Left
    Sleep    ${DELAY}
    Click Slide Bottom
    Sleep    ${DELAY}

Logout in RCTI Plus
    Click Page Profile
    Sleep    ${DELAY}
    Click Logout
    Sleep    ${DELAY}
    Close all browser
