*** Settings ***
Library    Selenium2Library
Resource  ../../Resources/loginKeywords.robot 
Resource  ../../Resources/exclusiveAllKeywords.robot
Resource  ../../Resources/logoutKeywords.robot 

*** Variables ***
${BROWSER}      headlesschrome
${URL}          http://rc-webm.rctiplus.com/login
${DELAY}        2s
${DELAY2}       25s
${USER}     ariefrahmanhakim0104@gmail.com
${PWD}     Testing@123456

*** Test Cases ***
Login in RCTI Plus
    Open My Browser    ${URL}    ${BROWSER}
    Enter UserName    ${USER}
    Enter Password    ${PWD}
    # Screenshot Login
    Click Login
    Wait Landing Page
    Sleep    ${DELAY}

Menu Exclusive All

    Click Page Exclusive
    Wait List Menu Exclusive
    Click All
    # Wait Video Exist
    Click Play Video
    Sleep    ${DELAY2}
    Click Pause Video
    Sleep    ${DELAY}
    Click Continue Video
    Sleep    ${DELAY}
    Click Close Video
    Sleep    ${DELAY}

Logout in RCTI Plus
    Click Page Profile
    Wait Page Profile
    # Screenshot Logout
    Click Logout
    Sleep    ${DELAY}
    Close all browser
