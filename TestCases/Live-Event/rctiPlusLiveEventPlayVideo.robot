*** Settings ***
Library    Selenium2Library
Resource  ../../Resources/loginKeywords.robot 
Resource  ../../Resources/liveEventPlayVideoKeywords.robot
Resource  ../../Resources/logoutKeywords.robot 

*** Variables ***
${BROWSER}      headlesschrome
${URL}          http://rc-webd.rctiplus.com/login
${DELAY}        1s
${DELAY2}       19s
${USER}     qa_test@mailinator.com
${PWD}     dikakoko

*** Test Cases ***
Login in RCTI Plus
    Open My Browser    ${URL}    ${BROWSER}
    Sleep     ${DELAY}
    Enter UserName    ${USER}
    Sleep     ${DELAY}
    Enter Password    ${PWD}
    Sleep    ${DELAY}
    Click Login
    Sleep    ${DELAY}

Menu Live Event Play Video

    Click Page Live Event
    Sleep    ${DELAY2}
    Click Pause Video
    Sleep    ${DELAY}
    Click Continue Video
    Sleep    ${DELAY2}
    Click Fullscreen
    Sleep    ${DELAY2}
    Press ESC
    Sleep    ${DELAY2}
    Click Pause2 Video
    Sleep    ${DELAY}
    Click Continue2 Video
    Sleep    ${DELAY}

Live Event Live

    Click Live Event Live
    Sleep    ${DELAY2}
    Click Pause3 Video
    Sleep    ${DELAY}
    Click Continue3 Video
    Sleep    ${DELAY2}
    Click Fullscreen2
    Sleep    ${DELAY2}
    Press ESC 2
    Sleep    ${DELAY2}
    Click Pause4 Video
    Sleep    ${DELAY}
    Click Continue4 Video
    Sleep    ${DELAY}

Logout in RCTI Plus
    Click Page Profile
    Sleep    ${DELAY}
    Click Logout
    Sleep    ${DELAY}
    Close all browser
