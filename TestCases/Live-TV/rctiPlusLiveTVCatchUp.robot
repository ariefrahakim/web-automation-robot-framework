*** Settings ***
Library    Selenium2Library
Resource  ../../Resources/loginKeywords.robot 
Resource  ../../Resources/liveTVCatchUpKeywords.robot
Resource  ../../Resources/logoutKeywords.robot 

*** Variables ***
${BROWSER}      headlesschrome
${URL}          http://rc-webd.rctiplus.com/login
${DELAY}        2s
${DELAY2}       23s
${DELAY3}       5s
${USER}     qa_test@mailinator.com
${PWD}     dikakoko

*** Test Cases ***
Login in RCTI Plus
    Open My Browser    ${URL}    ${BROWSER}
    Sleep     ${DELAY}
    Enter UserName    ${USER}
    Sleep     ${DELAY}
    Enter Password    ${PWD}
    Sleep    ${DELAY}
    # Screenshot Login
    Click Login
    Sleep    ${DELAY}
    Wait Landing Page


Menu Live TV Catch Up

    Click Page Live TV
    Sleep    ${DELAY}
    Press Page Down
    Sleep    ${DELAY}
    Click Catch Up Program
    Sleep    ${DELAY2}
    # Screenshot Catch Up Program Play
    Click Pause Video
    Sleep    ${DELAY}
    # Screenshot Catch Up Program Pause
    Click Continue Video
    Sleep    ${DELAY3}
    Click Fullscreen
    Sleep    ${DELAY3}
    # Screenshot Catch Up Program Fullscreen
    Press ESC
    Sleep    ${DELAY3}
    Click Pause2 Video
    Sleep    ${DELAY}
    Click Continue2 Video
    Sleep    ${DELAY3}
    Click Other Channels
    Sleep    ${DELAY3}
    # Screenshot Catch Up Other Channels

Logout in RCTI Plus
    Click Page Profile
    Sleep    ${DELAY}
    Wait Page Profile
    Sleep    ${DELAY}
    # Screenshot Logout 
    Click Logout
    Sleep    ${DELAY}
    Close all browser
