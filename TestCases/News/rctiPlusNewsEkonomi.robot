*** Settings ***
Library    Selenium2Library
Resource  ../../Resources/loginKeywords.robot 
Resource  ../../Resources/newsEkonomiKeywords.robot
Resource  ../../Resources/logoutKeywords.robot 

*** Variables ***
${BROWSER}      headlesschrome
${URL}          http://rc-webd.rctiplus.com/login
${DELAY}        1s
${DELAY2}       2s
${USER}     qa_test@mailinator.com
${PWD}     dikakoko

*** Test Cases ***
Login in RCTI Plus
    Open My Browser    ${URL}    ${BROWSER}
    Sleep     ${DELAY}
    Enter UserName    ${USER}
    Sleep     ${DELAY}
    Enter Password    ${PWD}
    Sleep    ${DELAY}
    Click Login
    Sleep    ${DELAY}

Menu News Ekonomi
    Click Page News
    Sleep    ${DELAY}
    Click Arrow Next
    Sleep    ${DELAY}
    Click Arrow Next 2
    Sleep    ${DELAY}
    Click Arrow Next 3
    Sleep    ${DELAY}
    Click Arrow Next 4
    Sleep    ${DELAY}
    Click Arrow Next 5
    Sleep    ${DELAY}
    Click Arrow Next 6
    Sleep    ${DELAY}
    Click Ekonomi
    Sleep    ${DELAY}
    Click Sample Berita
    Sleep    ${DELAY2}
    Press Page Down
    Sleep    ${DELAY2}
    Press Page Down 2
    Sleep    ${DELAY2}
    Click Artikel Asli
    Sleep    ${DELAY}

Close Browser
    Close all browser