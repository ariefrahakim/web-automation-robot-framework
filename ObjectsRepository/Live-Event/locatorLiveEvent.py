#Live Event Page Elements

menu_live_event = "xpath://body/nav[1]/div[1]/div[1]/ul[1]/li[3]/a[1]"
btn_pause = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]"
btn_continue = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]"
btn_fullscreen = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]"
live_event_live = "xpath://body/section[@id='live-non-fta-list']/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]"
btn_pause_live_event_list = "xpath://div[@id='fta-live']"
btn_continue_live_event_list = "xpath://div[@id='fta-live']"
btn_full_screen_live_event_list = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]"