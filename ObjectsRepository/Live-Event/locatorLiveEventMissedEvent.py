#Live Event Missed Event Page Elements

menu_live_event = "xpath://body/nav[1]/div[1]/div[1]/ul[1]/li[3]/a[1]"
missed_event = "xpath://body/section[@id='live']/div[1]/div[1]/ul[1]/li[2]/a[1]/h2[1]"
missed_event_list = "xpath://body/section[@id='missed-event-list']/div[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[1]"
btn_pause = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]"
btn_continue = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]"
btn_fullscreen = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]"
pop_up_error = "xpath://body/section[@id='live']/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/h4[1]"
