#Exclusive News Page Elements

menu_exclusive = "xpath://body/nav[1]/div[1]/div[1]/ul[1]/li[1]/a[1]"
menu_news = "xpath://body/section[@id='exclusive-menu']/div[1]/div[1]/ul[1]/div[1]/div[1]/div[5]/div[1]/li[1]/a[1]/h2[1]"
btn_play = "xpath://body/section[@id='feeds']/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/a[1]/img[1]"
btn_pause = "xpath://div[@id='player']"
btn_continue_play = "xpath://div[@id='player']"
btn_close_video = "xpath://body/div[@id='modalPlayerVIdJS']/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]"
