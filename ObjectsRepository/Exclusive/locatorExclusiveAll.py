#Exclusive All Page Elements

menu_exclusive = "xpath://body/nav[1]/div[1]/div[1]/ul[1]/li[1]/a[1]"
menu_all = "xpath://body/section[@id='exclusive-menu']/div[1]/div[1]/ul[1]/div[1]/div[1]/div[1]/div[1]/li[1]/a[1]/h2[1]"
btn_play = "//body/section[@id='feeds']/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/a[1]/img[1]"
btn_pause = "xpath://div[@id='player']"
btn_continue_play = "xpath://div[@id='player']"
btn_close_video = "xpath://body/div[@id='modalPlayerVIdJS']/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]"
list_menu_exclusive = "xpath://body/section[@id='exclusive-menu']/div[1]/div[1]/ul[1]/div[1]"
wait_video_play = "xpath://video[@id='player-272']"
# skip_ads = "xpath:/html/body/div[1]/div[3]/div/div[2]/button/div[1]"