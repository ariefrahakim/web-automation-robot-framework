import argparse
import robot

# from selenium import webdriver
# from webdriver_manager.chrome import ChromeDriverManager


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run Test Case")
    parser.add_argument("robotfile", type=str, help="robot file")

    args = parser.parse_args()

    robotfile: str = args.robotfile

    # driver = webdriver.Chrome(ChromeDriverManager().install())

    print(f"{robotfile=}")

    robot.run(robotfile)
